class CreateReceivemails < ActiveRecord::Migration
  def change
    create_table :receivemails do |t|
    	t.string :from
        t.string :from_mail
        t.string :to
	    t.string :to_mail
	      t.string :compact_string
	      t.text :sub
	      t.date :date
	    t.text :full_body
	    t.text :parsed_html_body
	    t.text :parsed_plain_body
	    t.integer :last_uid
	    t.integer :delete_check
	    t.integer :star_check
      t.boolean :read_unread_check
    end
  end
end
