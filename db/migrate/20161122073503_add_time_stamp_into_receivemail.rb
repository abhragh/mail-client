class AddTimeStampIntoReceivemail < ActiveRecord::Migration
  def change
  	add_timestamps(:receivemails)
  end
end
