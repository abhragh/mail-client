class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :name
      t.string :mail_id
      t.string :compact_string
      t.string :contact_no
      t.date :dob
      t.string :gender
      t.string :location
      t.timestamps null: false
    end
  end
end
