class CreateSentmail < ActiveRecord::Migration
  def change
    create_table :sentmails do |t|
      t.string :to
      t.string :cc
      t.string :bcc
      t.text :sub
      t.text :body
      t.text :body_text
      t.integer :star_check
      t.integer :delete_check
      t.timestamps null: false
    end
  end
end
