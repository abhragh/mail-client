ClientMail::Application.routes.draw do
  
  resources :contacts
  post 'contact/compose_mail'
  get 'contact/compose_mail'
  post 'contact/create' => 'contact#create'

  #-------------------Routes For Receivemail-----------------------#
  post 'sentmails/compose_mail'
  get 'sentmails/compose_mail'
  get 'sentmails/star'=>'sentmails#star'
  post 'sentmails/star'=>'sentmails#star'

  get 'sentmails/show' => 'sentmails#show'
  post 'sentmails/show' => 'sentmails#show'

  get 'sentmails/drafts'=>'sentmails#drafts'
  post 'sentmails/drafts'=>'sentmails#drafts'

  get 'sentmails/starred'=>'sentmails#starred'
  post 'sentmails/starred'=>'sentmails#starred'

  get 'sentmails/autocomplete'=>'sentmails#autocomplete'
  post 'sentmails/autocomplete'=>'sentmails#autocomplete'

  post 'sentmails/destroy_mail' => 'sentmails#destroy_mail'
  get 'sentmails/destroy_mail' => 'sentmails#destroy_mail'

  get 'sentmails/compose' => 'sentmails#compose_mail'
  get 'sentmails/sended' => 'sentmails#sended'
  post 'sentmails/create' => 'sentmails#create'
  #--------------------------------Routs for Receive mail---------------------------------#
  get 'receivemail/inbox' => 'receivemail#inbox'
  post 'receivemail/inbox' => 'receivemail#inbox'
  
  get 'receivemail/read'=>'receivemail#read'
  post 'receivemail/read'=>'receivemail#read'

  get 'receivemail/star'=>'receivemail#star'
  post 'receivemail/star'=>'receivemail#star'

  get 'receivemail/trash_mail'

  get 'receivemail/all_mail'
  post 'receivemail/all_mail'

  
  get 'receivemail/show' => 'receivemail#show'

  get 'receivemail/delete'=>'receivemail#delete'
  post 'receivemail/delete'=>'receivemail#delete'
 
  get 'receivemail/trash_mail' => 'receivemail#trash_mail'	
  post'receivemail/trash_mail' => 'receivemail#trash_mail'  
 
 #get '/:id' => "shortener/shortened_urls#show"
  
  #root 'receivemail#inbox'
  root :to => 'receivemail#inbox'
  
end
